﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

[assembly: log4net.Config.XmlConfigurator(ConfigFile = "log4net.xml", Watch = true)]

namespace FindFileTask
{
	/// <summary>
	/// Задание реализовано частично ввиду ограниченного кол-ва свободного времени
	/// async/await в общем использую впервые
	/// </summary>
	public partial class FindFile : Form
	{
		CancellationTokenSource cancelAcync = null;
		public static ILog log;

		public FindFile()
		{
			InitializeComponent();
			log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
			SetPrevInputState();
		}

		private void chooseDirBtn_Click(object sender, EventArgs e)
		{
			var a = new FolderBrowserDialog();
			if (a.ShowDialog() == DialogResult.OK)
			{
				choosenDir.Text = a.SelectedPath;
			}
		}

		private async void findBtn_Click(object sender, EventArgs e)
		{
			SaveInput();
			ControlsToDefault();

			var patternFileName = filePatternBox.Text;
			if (String.IsNullOrEmpty(patternFileName))
			{
				patternFileName = "*.*";
			}

			var wordsToFind = wordsToFindBox.Text;
			cancelAcync = new CancellationTokenSource();
			try
			{
				var awaiter = await FindFiles(patternFileName, wordsToFind, cancelAcync.Token);
			}
			catch (Exception ep)
			{
				log.Error(ep);
			}
		}

		private async Task<Int32> FindFiles(String patternFileName, String wordsToFind, CancellationToken ct)
		{
			var p = await helpAcync(patternFileName, wordsToFind,ct);
			return 1;
		}

		private Task<Int32> helpAcync(String patternFileName, String wordsToFind, CancellationToken ct)
		{
			return Task.Run(() =>
				{
					String root = choosenDir.Text;
					if (!Directory.Exists(root))
					{
						throw new ArgumentException();
					}

					var dirs = new Stack<String>();
					dirs.Push(root);
					while (dirs.Count > 0)
					{
						var currentDir = dirs.Pop();

						String[] subDirs;
						try
						{
							subDirs = Directory.GetDirectories(currentDir);
						}
						catch (Exception ep)
						{
							log.Warn(ep);
							continue;
						}

						foreach (var str in subDirs)
							dirs.Push(str);

						String[] files;
						try
						{
							files = Directory.GetFiles(currentDir, patternFileName);
						}
						catch (Exception ep)
						{
							log.Warn(ep);
							continue;
						}

						ProcessPartOfFiles(files, wordsToFind);
					}
					return 0;
				});
		}

		private void ProcessPartOfFiles(String[] files, String wordsToFind)
		{
			var goodExtensions = new List<String>() { ".txt", ".doc", ".docx" };
			TreeNode root = new TreeNode();
			this.BeginInvoke((Action)(() =>
				{
					this.resultView.Nodes.Add(root);
				})); 
			foreach (var file in files)
			{
				var currentFile = new FileInfo(file);
				filesProcessed.Value += 1;
				if (!goodExtensions.Contains(currentFile.Extension)) continue;
				this.BeginInvoke((Action)(() =>
				{
					processedFileBox.Text = currentFile.FullName;
				}));

				List<String> fileContent = new List<string>();
				if(currentFile.Extension==".txt")
				 fileContent = File.ReadAllLines(currentFile.FullName, Encoding.Default).ToList();
				else fileContent = File.ReadAllLines(currentFile.FullName, Encoding.UTF8).ToList();

				if (String.IsNullOrEmpty(wordsToFind) || fileContent.Contains(wordsToFind))
				{
					 AddNode(file);
				}
			}
		}

		private void AddNode(String file)
		{
			TreeNode node = resultView.Nodes[0];
			foreach (string pathBits in file.Split('\\'))
			{
				if (node.Nodes.ContainsKey(pathBits))
				{
					this.BeginInvoke((Action)(() =>
					{
						node = node.Nodes[pathBits];
					}));
				}
				else
				{
					this.BeginInvoke((Action)(() =>
					{
						node = node.Nodes.Add(pathBits, pathBits);
					}));
				}
			}
		}

		private void showPatternInfo_Click(object sender, EventArgs e)
		{
			MessageBox.Show(@"Шаблоны представляют собой обычное имя файла (и его расширение), в котором вместо некоторых букв появляются знаки ?, *, и []. Знак вопроса указывает на то, что на его месте непременно должен стоять какой-либо допустимый символ имени файла (буква, цифра и т.д.) и причем только один . Знак * указывает на то, что на его месте может стоять любое количество символов , допустимых в имени файла, а также вообще ничего не стоять . В квадратные скобки заключаются те знаки, один из которых обязательно должен присутствовать в имени файла на этом месте.");
		}

		private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
		{
			MessageBox.Show("Поиск текстовых файлов, вывод их в treeview, ошибки в логи.");
		}

		private void stopFind_Click(object sender, EventArgs e)
		{
			if (cancelAcync != null)
			{
				cancelAcync.Cancel();
			}
		}

		private void ControlsToDefault()
		{
			processedFileBox.Text = "";
			filesProcessed.Value = 0;
			resultView.Nodes.Clear();
		}

		private void SaveInput()
		{
			Properties.Settings.Default.filePattern = filePatternBox.Text;
			Properties.Settings.Default.directory = choosenDir.Text;
			Properties.Settings.Default.textToFind = wordsToFindBox.Text;
			Properties.Settings.Default.Save();
		}

		private void SetPrevInputState()
		{
			choosenDir.Text = Properties.Settings.Default.directory;
			filePatternBox.Text = Properties.Settings.Default.filePattern;
			wordsToFindBox.Text = Properties.Settings.Default.textToFind;
		}

	}
}
