﻿namespace FindFileTask
{
	partial class FindFile
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.chooseDirBtn = new System.Windows.Forms.Button();
			this.choosenDir = new System.Windows.Forms.TextBox();
			this.filePatternBox = new System.Windows.Forms.TextBox();
			this.showPatternInfo = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.findBtn = new System.Windows.Forms.Button();
			this.processedFileBox = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.filesProcessed = new System.Windows.Forms.NumericUpDown();
			this.resultView = new System.Windows.Forms.TreeView();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.оПрограммеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.wordsToFindBox = new System.Windows.Forms.TextBox();
			this.stopFind = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.filesProcessed)).BeginInit();
			this.menuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// chooseDirBtn
			// 
			this.chooseDirBtn.Location = new System.Drawing.Point(13, 45);
			this.chooseDirBtn.Name = "chooseDirBtn";
			this.chooseDirBtn.Size = new System.Drawing.Size(79, 42);
			this.chooseDirBtn.TabIndex = 0;
			this.chooseDirBtn.Text = "Выбрать директорию";
			this.chooseDirBtn.UseVisualStyleBackColor = true;
			this.chooseDirBtn.Click += new System.EventHandler(this.chooseDirBtn_Click);
			// 
			// choosenDir
			// 
			this.choosenDir.Location = new System.Drawing.Point(98, 57);
			this.choosenDir.Name = "choosenDir";
			this.choosenDir.ReadOnly = true;
			this.choosenDir.Size = new System.Drawing.Size(202, 20);
			this.choosenDir.TabIndex = 1;
			// 
			// filePatternBox
			// 
			this.filePatternBox.Location = new System.Drawing.Point(98, 121);
			this.filePatternBox.MaxLength = 100;
			this.filePatternBox.Name = "filePatternBox";
			this.filePatternBox.Size = new System.Drawing.Size(202, 20);
			this.filePatternBox.TabIndex = 2;
			// 
			// showPatternInfo
			// 
			this.showPatternInfo.Location = new System.Drawing.Point(13, 109);
			this.showPatternInfo.Name = "showPatternInfo";
			this.showPatternInfo.Size = new System.Drawing.Size(79, 42);
			this.showPatternInfo.TabIndex = 3;
			this.showPatternInfo.Text = "О шаблонах";
			this.showPatternInfo.UseVisualStyleBackColor = true;
			this.showPatternInfo.Click += new System.EventHandler(this.showPatternInfo_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(98, 105);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(84, 13);
			this.label1.TabIndex = 4;
			this.label1.Text = "Шаблон файла:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(13, 158);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(149, 13);
			this.label2.TabIndex = 6;
			this.label2.Text = "Текст для поиска в файлах:";
			// 
			// findBtn
			// 
			this.findBtn.Location = new System.Drawing.Point(13, 285);
			this.findBtn.Name = "findBtn";
			this.findBtn.Size = new System.Drawing.Size(79, 42);
			this.findBtn.TabIndex = 7;
			this.findBtn.Text = "Найти";
			this.findBtn.UseVisualStyleBackColor = true;
			this.findBtn.Click += new System.EventHandler(this.findBtn_Click);
			// 
			// processedFileBox
			// 
			this.processedFileBox.Location = new System.Drawing.Point(380, 57);
			this.processedFileBox.Name = "processedFileBox";
			this.processedFileBox.ReadOnly = true;
			this.processedFileBox.Size = new System.Drawing.Size(202, 20);
			this.processedFileBox.TabIndex = 8;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(377, 41);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(130, 13);
			this.label3.TabIndex = 9;
			this.label3.Text = "Обрабатываемый файл:";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(377, 105);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(113, 13);
			this.label4.TabIndex = 10;
			this.label4.Text = "Файлов обработано:";
			// 
			// filesProcessed
			// 
			this.filesProcessed.Location = new System.Drawing.Point(380, 122);
			this.filesProcessed.Maximum = new decimal(new int[] {
            1316134912,
            2328,
            0,
            0});
			this.filesProcessed.Name = "filesProcessed";
			this.filesProcessed.ReadOnly = true;
			this.filesProcessed.Size = new System.Drawing.Size(202, 20);
			this.filesProcessed.TabIndex = 11;
			// 
			// resultView
			// 
			this.resultView.Location = new System.Drawing.Point(380, 173);
			this.resultView.Name = "resultView";
			this.resultView.Size = new System.Drawing.Size(356, 442);
			this.resultView.TabIndex = 14;
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.оПрограммеToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(748, 24);
			this.menuStrip1.TabIndex = 15;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// оПрограммеToolStripMenuItem
			// 
			this.оПрограммеToolStripMenuItem.Name = "оПрограммеToolStripMenuItem";
			this.оПрограммеToolStripMenuItem.Size = new System.Drawing.Size(94, 20);
			this.оПрограммеToolStripMenuItem.Text = "О программе";
			this.оПрограммеToolStripMenuItem.Click += new System.EventHandler(this.оПрограммеToolStripMenuItem_Click);
			// 
			// wordsToFindBox
			// 
			this.wordsToFindBox.Location = new System.Drawing.Point(13, 173);
			this.wordsToFindBox.MaxLength = 500;
			this.wordsToFindBox.Multiline = true;
			this.wordsToFindBox.Name = "wordsToFindBox";
			this.wordsToFindBox.Size = new System.Drawing.Size(287, 106);
			this.wordsToFindBox.TabIndex = 5;
			// 
			// stopFind
			// 
			this.stopFind.Location = new System.Drawing.Point(221, 285);
			this.stopFind.Name = "stopFind";
			this.stopFind.Size = new System.Drawing.Size(79, 42);
			this.stopFind.TabIndex = 16;
			this.stopFind.Text = "Остановить поиск";
			this.stopFind.UseVisualStyleBackColor = true;
			this.stopFind.Click += new System.EventHandler(this.stopFind_Click);
			// 
			// FindFile
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(748, 632);
			this.Controls.Add(this.stopFind);
			this.Controls.Add(this.resultView);
			this.Controls.Add(this.processedFileBox);
			this.Controls.Add(this.filesProcessed);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.findBtn);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.wordsToFindBox);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.showPatternInfo);
			this.Controls.Add(this.filePatternBox);
			this.Controls.Add(this.choosenDir);
			this.Controls.Add(this.chooseDirBtn);
			this.Controls.Add(this.menuStrip1);
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "FindFile";
			this.Text = "Поиск файлов";
			((System.ComponentModel.ISupportInitialize)(this.filesProcessed)).EndInit();
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button chooseDirBtn;
		private System.Windows.Forms.TextBox choosenDir;
		private System.Windows.Forms.TextBox filePatternBox;
		private System.Windows.Forms.Button showPatternInfo;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button findBtn;
		private System.Windows.Forms.TextBox processedFileBox;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.NumericUpDown filesProcessed;
		private System.Windows.Forms.TreeView resultView;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem оПрограммеToolStripMenuItem;
		private System.Windows.Forms.TextBox wordsToFindBox;
		private System.Windows.Forms.Button stopFind;
	}
}

